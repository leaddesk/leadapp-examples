(function() {
  // configuration
  var WARN_THRESHOLD = 0.75; // 0...1, percentage of timeout when warning is shown
  var MAX_IDLE_TIME = url("?time") || 60; // seconds

  var CALL_LINE_STATE = "call_line_state";
  var CALL_DIALOG_VISIBLE = "call_dialog_visible";

  var _timeout;
  var _callOnline = false;
  var _dialogOpen = false;

  // Initialize post message handling
  var _post = LeadDesk.PostMessageClient;
  _post.send("observe_events", { types: ["call_line", "call_dialog"] });
  _post.onMessage(handlePostMessage);
  _post.get(CALL_DIALOG_VISIBLE).then(function(result) {
    _dialogOpen = Boolean(result.value);
  });
  _post.get(CALL_LINE_STATE).then(function(result) {
    _callOnline = result.value === "callLineOn";
  });

  /**
   * Show warning notification using POST message
   */
  function showWarningNotification() {
    $("body").addClass("warn");
    _post.send("notify", {
      message: "Time is running out. Hurry up!",
      style: {
        icon: "glyphicons-warning-sign",
        text_color: "black",
        background_color: "yellow"
      }
    });
    _timeout = setTimeout(
      showErrorNotification,
      MAX_IDLE_TIME * 1e3 * (1 - WARN_THRESHOLD)
    );
  }

  /**
   * Show error notification using POST message
   */
  function showErrorNotification() {
    $("body")
      .removeClass("warn")
      .addClass("error");
    _post.send("notify", {
      message: "You have used your time. Move to the next call NOW!",
      style: {
        icon: "glyphicons-warning-sign",
        text_color: "white",
        background_color: "red"
      }
    });
  }

  /**
   * Go into good state where idle time is not monitored
   */
  function setGoodState() {
    cancel();
    $("body").removeClass("warn error");
  }

  /**
   * Cancel idle time monitoring
   */
  function cancel() {
    clearTimeout(_timeout);
  }

  /**
   * Handle events from machine
   * @param {object} data event data
   */
  function handlePostMessage(data) {
    $("body").removeClass("warn error");
    switch (data.type) {
      case "callDialogOpened":
        _dialogOpen = true;
        break;

      case "callDialogClosing":
        setGoodState();
        _dialogOpen = false;
        break;

      case "callLineOn":
        setGoodState();
        _callOnline = true;
        break;

      case "callLineRinging":
        setGoodState();
        break;

      case "callLineIdle":
        if (_callOnline && _dialogOpen) {
          _timeout = setTimeout(
            showWarningNotification,
            MAX_IDLE_TIME * 1e3 * WARN_THRESHOLD
          );
          _callOnline = false;
        }
        break;
    }
  }
})();
