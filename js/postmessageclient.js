var LeadDesk = LeadDesk || {};

/**
 * LeadDesk machine post message client
 *
 * Can be used to send and receive post messages to/from LeadDesk machine within
 * an iframe if machine is the parent window.
 */
LeadDesk.PostMessageClient = (function() {
  var _messageCallback;
  var _bindDone = false;
  var _id = 0;
  var _getRequests = {};
  var self = {};

  function _bind() {
    if (_bindDone) {
      return;
    }
    // Create IE + others compatible event handler
    var eventMethod = window.addEventListener
      ? "addEventListener"
      : "attachEvent";
    var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
    var eventListener = window[eventMethod];

    // Listen to message from child window / iframe
    eventListener(messageEvent, _handleMessage, false);
    _bindDone = true;
  }

  function _handleMessage(event) {
    try {
      var message = JSON.parse(event.data);

      // handle getResponse messages
      if (
        message.type &&
        message.type === "getResponse" &&
        _getRequests[message.data.id]
      ) {
        var getId = message.data.id;
        delete message.data.id;
        _getRequests[getId](message.data);
        delete _getRequests[getId];

        // forward other messages to listener
      } else if (_messageCallback) {
        _messageCallback(message, event);
      }
    } catch (e) {
      console.warn(e, event.data);
    }
  }

  /**
   * Callback definition of onMessageCallback
   *
   * @callback onMessageCallback
   * @param {object} message Message sent from machine
   * @param {object} event Original post message event
   */

  /**
   * Register a post message listener for messages and events incoming from machine
   *
   * @param {onMessageCallback} callback Called when a post message is received
   */
  self.onMessage = function(callback) {
    _bind();
    _messageCallback = callback;
  };

  /**
   * Send a message to the LeadDesk machine
   *
   * @param {string} action Request action from machine
   * @param {object} [params={}] Action parameters
   * @param {string} [targetOrigin=*] Target origin filter
   */
  self.send = function(action, params, targetOrigin) {
    this.sendRaw(
      JSON.stringify({
        action: action,
        params: params || {}
      }),
      targetOrigin || "*"
    );
  };

  /**
   * Get property value from machine
   *
   * @param {string} name Name of the property
   * @param {string} [targetOrigin=*] Target origin filter
   * @return {Promise} Will be resolved with the response from machine or rejected after a short timeout.
   */
  self.get = function(name, targetOrigin) {
    _bind();
    return new Promise(function(resolve, reject) {
      var id = "get-" + ++_id;
      _getRequests[id] = resolve;
      targetOrigin = targetOrigin || "*";
      window.parent.postMessage(
        JSON.stringify({
          action: "get",
          params: {
            id: id,
            name: name
          }
        }),
        targetOrigin
      );

      setTimeout(function() {
        delete _getRequests[id];
        reject({ reason: "timeout" });
      }, 2000);
    });
  };

  /**
   * Send a raw post message to machine
   *
   * @param {*} message Message to be sent
   * @param {string} [targetOrigin=*] Target origin filter
   */
  self.sendRaw = function(message, targetOrigin) {
    targetOrigin = targetOrigin || "*";
    window.parent.postMessage(message, targetOrigin);
  };

  return self;
})();
